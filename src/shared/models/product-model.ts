import { BrandModel } from './brand-model';
import { ProductTypeModel } from './product-type-model';
export class ProductModel {
  id: string;
  name: string;
  code: string;
  price: number;
  description: string;
  quantity: number;
  productTypeId: ProductTypeModel;
  brandId: BrandModel;
  createdDate: Date;

  public constructor() {
    this.id = '';
    this.name = '';
    this.code = '';
    this.price = 0;
    this.description = '';
    this.quantity = 0;
    this.productTypeId = new ProductTypeModel();
    this.brandId = new BrandModel();
    this.createdDate = null;

  }
}
