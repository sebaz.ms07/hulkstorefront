import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BrandRoutingModule } from './brand-routing.module';
import { CreateBrandComponent } from './create-brand/create-brand.component';


@NgModule({
  declarations: [CreateBrandComponent],
  imports: [
    CommonModule,
    BrandRoutingModule
  ]
})
export class BrandModule { }
